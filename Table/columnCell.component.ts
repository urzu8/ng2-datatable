import { Component, Input, OnInit, ViewChild, ElementRef } from "@angular/core";
import { UtilityUIService } from "../../../Modules/Utility/Services/Utility.UI.service";

@Component({
    selector: "[table-cell]",
    templateUrl: "app/Shared/DataTable/Table/columnCell.html"
})
export class ColumnCellComponent implements OnInit {
    @Input("cell-data") cellData;
    @Input("cell-config") cellConfig;

    @ViewChild('dataContainer') dataContainer: ElementRef;

    ngOnInit(){
        //console.log("cell spam!!", this.cellConfig.columnName, this.cellConfig.map, typeof this.cellConfig.map);
    }

    getData(){
        let data = this.cellData[this.cellConfig.columnName];
        if(this.cellConfig.type) return this.getFormatted();
        
        //if(typeof mapFunc === "function") return  mapFunc(data);
        return data;
    }

    getFormatted(){
        switch(this.cellConfig.type){
            case "date":
                return this.date();
            case "colorBar":
                return this.buildColorBar();
            case "money":
                return this.money();
            case "custom":
                return this.cellConfig.map();
        }
    }

    private date(){
        let date = this.cellData[this.cellConfig.columnName];
        let fDate = new Date(date);
        let month: any = fDate.getMonth() + 1;
        month = month <= 9 ? "0" + month : month;
        let day: any = fDate.getDate();
        day = day <= 9 ? "0" + day : day;
        
        return month + "/" + day + "/" + fDate.getFullYear();
    }

    private buildColorBar(){
        let html = '<span class="label ' + this.ui.getStatusColor(this.cellData[this.cellConfig.columnName]) + '">' + this.cellData[this.cellConfig.columnName] + '</span>'
        if(this.dataContainer) this.dataContainer.nativeElement.innerHTML = html;
    }

    private money(){
        return '$' + parseFloat(this.cellData[this.cellConfig.columnName]).toFixed(2);
    }

    

    constructor(private ui: UtilityUIService){

    }

}
