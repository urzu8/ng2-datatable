﻿import { Component, OnInit, Input, ViewChild } from "@angular/core";
import { Router } from "@angular/router";

import { DataManagerService } from "../DataManager/dataManager.service";
import { InfiniteScroll } from "angular2-infinite-scroll";

import { ExportService } from "../../Core/DataLayer/Services/Export.service";

import { ContextMenuService,
    ContextMenuComponent } from "angular2-contextmenu/angular2-contextmenu";

@Component({
    selector: "[data-table]",
    templateUrl: "app/Shared/DataTable/Table/dataTable.html",
    styles: [`
        .glyphicon.fast-right-spinner {
        -webkit-animation: glyphicon-spin-r 1s infinite linear;
        animation: glyphicon-spin-r 1s infinite linear;
        }
        .table-hover > tbody > tr:hover {
            background-color: #ebd8d6;
        }
        .table-hover > thead > tr > td:hover {
            background-color: #ebd8d6;
        }
    `],
    providers: [DataManagerService]
})



export class DataTableComponent implements OnInit{
    sortDir: string = "";

    
    @Input("isLoading") isLoading;
    @Input("contextMenuItems") contextMenuItems;
    @Input("table-config") tableConfig;
    @Input("dataLoad") dataLoad;
    @Input("summaryLoad") summaryLoad;
    @ViewChild(ContextMenuComponent) public basicMenu: ContextMenuComponent;

    //expose windowScroll for IS, also a height so that it'll work

    ngOnInit() {

    }

    start(){
        setTimeout(() => {
            this.dataManager.setLoad(this.dataLoad);
            this.dataManager.setSummary(this.summaryLoad);
            this.dataManager.startDataTable(this.tableConfig);
        }, 0);
    }

    public onFiltersUpdated(){
        this.dataManager.resetTable();
    }

    public onContextMenu($event: MouseEvent, item: any): void {
        this.contextMenuService.show.next({ 
            actions: this.contextMenuItems,
            event: $event, 
            item: item });
        $event.preventDefault();
        $event.stopPropagation();
    }

    exportTableData(){

        this.exportService.exportData(this.dataManager.currData);
    }

    constructor(private dataManager: DataManagerService,
        private contextMenuService: ContextMenuService,
        private exportService: ExportService,
        private router: Router) {}

    printMe(datas) {

    }
}

