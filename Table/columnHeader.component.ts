import { Component, Input } from "@angular/core";
import { DataManagerService } from "../DataManager/dataManager.service";

@Component({
    selector: "[column-header]",
    templateUrl: "app/Shared/DataTable/Table/columnHeader.html"
})
export class ColumnHeaderComponent {

    sortDir: string = "";
    icon: string = "";

    @Input("column-header") column;

    onSortClicked(columnName){
        if (!this.column.sortable) return;

        this.toggleSortDir();
        
        this.dataManager.setSorter(columnName + " " + this.sortDir);
        
        this.updateIcon();
    }

    toggleSortDir(){
        switch(this.sortDir){
            case "":
                console.log("sortDir ");
                this.sortDir = "asc";
                break;
            case "asc":
                console.log("sortDir asc");
                this.sortDir = "desc";
                break;
            case "desc":
                console.log("sortDir desc");
                this.sortDir = "asc";
                break;
            default:
                console.log("sortDir default");
                this.sortDir = "asc";
                break;

        }
    }

    test(n: string){
        if(n == this.sortDir) return true;
        return false;

    }

    updateIcon(){
        if(this.sortDir == "asc") this.icon =  "glyphicon 'glyphicon-chevron-up'";
        if(this.sortDir == "desc") this.icon = "glyphicon 'glyphicon-chevron-down'";
    }

    isAsc(){
        
        return this.sortDir == 'asc';
    }

    isDesc(){
        
        return this.sortDir == 'desc';
    }

    constructor(private dataManager: DataManagerService){}
}