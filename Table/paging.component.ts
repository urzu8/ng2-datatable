import { Component, Input } from "@angular/core";

import { DataManagerService } from "../DataManager/dataManager.service";

@Component({
    selector: "paging",
    templateUrl: "app/Shared/DataTable/Table/paging.html",
    styles: [`
        ul{ list-style-type: none; }
        ul li{ list-style-type: none; }
        .pagination li {
            display: inline-block;
            border-radius: 2px;
            text-align: center;
            vertical-align: top;
            height: 30px;
        }
        .pagination li.active {
            background-color: #ee6e73;
        }
        .waves-effect {
            position: relative;
            cursor: pointer;
            display: inline-block;
            overflow: hidden;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            -webkit-tap-highlight-color: transparent;
            vertical-align: middle;
            z-index: 1;
            will-change: opacity, transform;
            transition: .3s ease-out;
        }
    `]
})
export class PagingComponent{
    currentPage: number = 0;
    totalPages: number = 10;

    @Input("pageSize") pageSize: number;

    setPageSize(num: number){}

    counter(length: number){
        let arr = [];
        for(let i = 1;i <= 10;i++){
            arr.push(i);
        }
        return arr;
    }

    setCurrentPage(page: number){
        this.currentPage = page;
        this.dataManager.setCurrentPage(page);
    }

    calcNumPages(){}

    constructor(private dataManager: DataManagerService){}

}


