﻿
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";

import {LPSelectModule} from '../Select/select.module';

import { UIModule } from "../ui/ui.module";

import { DataManagerService } from "./DataManager/dataManager.service";
import { DataLayerModule } from "../Core/DataLayer/dataLayer.module";
import { DataTableComponent } from "./Table/dataTable.component";
import { PagingComponent } from "./Table/paging.component";
import { ColumnHeaderComponent } from "./Table/columnHeader.component";
import { ColumnCellComponent } from "./Table/columnCell.component";

import { FilterManagerService } from "./Filters/filterManager.service";
import { FilterListComponent } from "./Filters/Components/filterList.component";


import { FilterBaseComponent } from "./Filters/Components/Controls/filterBase.component";
import { DateFilterComponent } from "./Filters/Components/Controls/Date/dateFilter.component";
import { SelectFilterComponent } from "./Filters/Components/Controls/Select/selectFilter.component";
import { TextFilterComponent } from "./Filters/Components/Controls/Text/textFilter.component";
import { FilterDataService } from "./Filters/filterData.service";

import {ContextMenuModule} from "angular2-contextmenu/angular2-contextmenu";
import { InfiniteScrollModule } from "angular2-infinite-scroll/angular2-infinite-scroll";

@NgModule({
    imports: [
        CommonModule,
        DataLayerModule,
        LPSelectModule,
        ContextMenuModule,
        InfiniteScrollModule,
        RouterModule,
        FormsModule,
        UIModule
    ],
    providers: [FilterManagerService, FilterDataService],
    declarations: [
        DataTableComponent,
        FilterListComponent,
        FilterBaseComponent,
        DateFilterComponent,
        SelectFilterComponent,
        TextFilterComponent,
        PagingComponent,
        ColumnHeaderComponent,
        ColumnCellComponent
    ],
    exports: [DataTableComponent, FilterListComponent]
})

export class DataTableModule { }