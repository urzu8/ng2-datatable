﻿import { Injectable, Injector, OnInit } from '@angular/core';

import { I_SearchParams } from "../../ORM/SearchParams.orm";

import { FilterManagerService } from "../Filters/filterManager.service";

import { UtilityDataService } from "../../../Modules/Utility/Services/Utility.Data.services";
import { I_JSON } from "../../ORM/Utility.orm";



@Injectable()
export class DataManagerService {

    tableData: any[] = [];
    currData: any[] = [];
    summaryData: any;
    config: any = {};
    search: I_SearchParams;
    numPage: number = 0;
    isLoading: boolean = false;
    infiniteScroll: boolean = false;
    currentPage: number = 1;
    loading: boolean = true;

    itemsPerPage: number = 50;
    tableLoadFunc: any;
    summaryLoadFunc: any;

    currSorter: string;
    sortDirection: string;

    ngOnInit() {
    }

    setSorter(sorter: string, reset?: boolean){
        if(reset) this.currSorter = this.config.sorter.defaultSorter;
        else this.currSorter = sorter;
        this.resetTable();
    }

    resetTable(){
        this.tableData = [];
        this.numPage = 0;
        this.currentPage = 1;
        this.getPageData();
    }
    
    setCurrentPage(pageNum: number){
        this.currentPage = pageNum;
        this.getPageData();
    }

    getPageData(){
        console.log("loading page", this.currentPage - 1);
        if(this.infiniteScroll){
            this.currData =  this.tableData;
        }
        else{
            console.log("getPageData", this.tableData, this.currentPage);
            if(this.tableData[this.currentPage - 1] && this.tableData[this.currentPage - 1].index){
                this.currData = [this.tableData[this.currentPage - 1]];
            }
            else{
                console.log("page not found, fetching data");
                this.loading = true;
                this.tableLoadFunc(this.currentPage - 1)
                .then(() => {
                    this.loading = false;
                    console.log("fetched data", this.tableData);
                    this.currData = [this.tableData[this.currentPage - 1]];
                })
                
            }
        }
    }


    onScroll() {
        if (!this.isLoading && this.infiniteScroll) {
            this.numPage++;
            this.isLoading = true;
            this.tableLoadFunc(this.numPage)
            .then(() => this.isLoading = false);
        }
    }

    setLoad(func: any) {
        console.log("Func : ", func);
        this.tableLoadFunc = (page: number) => {
            var params: I_SearchParams = new I_SearchParams();
            return this.getSearchParams(page)
            .then((params) => this.filterService.getFilterData(params))
            .then((params) => { console.log('calling func', this); return func(params)})
            .then((data) => this.addPage(data));
        
        };
    }

    setSummary(func: any){
        if(func){
        this.summaryLoadFunc = () => {
            var params: I_SearchParams = new I_SearchParams();
            return this.getSearchParams(0)
            .then((params) => this.filterService.getFilterData(params))
            .then((params) => func(params))
            .then((data) => {
                this.summaryData = data;
            })
                }
        }
        else{
            this.summaryLoadFunc = () =>{
             this.summaryData = {
                    Items: [],
                    Record_Count: 0
                }
            }
        }
    }

    getSummaryData(){
        
        if(typeof this.summaryLoadFunc === "function") this.summaryLoadFunc();
    }

    findSummary(colName){
        for (var i in this.summaryData.Items){
            if(this.summaryData.Items[i].Column == colName) return this.summaryData.Items[i].Value;
        }
        return "";
    }

    startDataTable(config){
        this.setConfig(config)
        .then(() => this.setCurrentPage(1))

        this.getSummaryData();
        
    }

    addPage(data: any[]) {
        let pageNum = this.tableData.length > 0 ? this.tableData.length + 1 : 0;
        let index: string = "${pageNum}";
        if(this.tableData.length < this.currentPage - 1) this.buffout();

        if(this.tableData.length > this.currentPage )
            this.tableData[this.currentPage - 1] = { index: data }
        else
            this.tableData.push({ index: data });
        console.log("table data dump",this.tableData, this.config);
        
    }

    buffout(){//buff the page numbers if we click ahead
        let n = this.currentPage - this.tableData.length;
        for(let i = 0; i < n - 1;i++)
            this.tableData.push({index: false});
    }

    getSearchParams(pageNum: number): Promise<I_SearchParams> {
        this.search.Past = pageNum;
        this.search.Top = this.itemsPerPage;
        this.search.Past = pageNum;
        this.search.Sorter = this.currSorter || "";
        this.search.Phrase = "";
        this.search.Division_ID = 1;//user.division
        return Promise.resolve(this.search);
    }

    setConfig(configTitle): Promise<any> {
        let payload = new I_JSON();
        payload.JSON_Title = configTitle;

        return this.utilityService.fetchJSONByTitle(payload)
        .then((config) => {
            this.config = JSON.parse(config.JSON);
            this.infiniteScroll = this.config.infiniteScroll;
            this.currSorter = this.config.sorter.defaultSorter;
        })
        .catch((err) => console.error("Failed to fetch config in DataTable", err))
    }

    

    constructor(private utilityService: UtilityDataService, private filterService: FilterManagerService) {
        this.search = new I_SearchParams();
        //this should be moved into a func setDefaultParams to be called from job component
        this.search.Top = this.itemsPerPage;
        this.search.Past = 1;
        this.search.Sorter = "";
        this.search.Phrase = "";
        this.search.Division_ID = 1;//user.division

                        
            
        
    }
}