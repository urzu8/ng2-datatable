# Data Table

## Note

As with my ng2-select, this was built to meet the needs of of a web application from previous work. I have not taken the time to repair the CSS lost.

## Description

We needed a JSON customizable, repeatable solution that could handle many different grid views based on different data entities. The table can be configured for both infinite scroll and more traditional paging. Filtering was built in to meet internal needs, but can be disabled through config. Table cells can be given custom templates to accomplish any amount of styling.

## example config

    export const JobTableConfig = {
        columns: [
            { columnName: "Job_Identifier", alias: "Job #", sorterColumn: "Job_ID", sortable: true, link: true, linkUrl:"/jobs/view/" },
            { columnName: "Client_ID", alias: "Company", sorterColumn: "Client_ID", sortable: true, link: true, linkUrl:"/company/view/" },
            { columnName: "Job_Name", alias: "Job Name", sorterColumn: "Job_Name", sortable: false, link: false },
            { columnName: "STR", alias: "STR", sorterColumn: "STR", sortable: false, link: false, summary: true},
            { columnName: "Start_Date", alias: "test date", sorterColumn: "Start_Date", sortable: true, link: false, type: "date" },
            { columnName: "State_ID", alias: "State", sorterColumn: "State_ID", sortable: true, link: false },
            { columnName: "Job_Status_ID", alias: "Status", sorterColumn: "Job_Status_ID", sortable: false, link: false, summary: true },
            { columnName: "Survey_Type_ID", alias: "Job Type", sorterColumn: "Survey_Type_ID", sortable: false, link: false }
        ],
        sorter: {
            direction: "desc",
            defaultSorter: "Job_ID desc"
        },
        infiniteScroll: false,
        itemsPerPage: 50
    }



    export const JobTableFilters = {
        filters: [
            {
                label: "Employee Select",
                placeholder: "No Selection",
                type: "select",
                dataSource: "api/Employee",
                dataParams: {},
                pkidColumnName: "EmployeeId",
                textColumnName: "LastName",
                value: [0],
                iSearchFilterTarget: "Employees"
            },
            {
                label: "Start Date",
                type: "date",
                dataSource: "",
                dataParams: {},
                pkidColumnName: "",
                textColumnName: "",
                value: "1/1/1900",
                iSearchFilterTarget: "StartDate"
            },
            
            {
                label: "End Date",
                type: "date",
                dataSource: "",
                dataParams: {},
                pkidColumnName: "",
                textColumnName: "",
                value: "1/1/2199",
                iSearchFilterTarget:"EndDate"
            },
        ]
    }


## Usage

    <div class="panel panel-flat"
    data-table 
        [contextMenuItems]="contextMenuItems"
        table-config="lookup.database.blob.config.string"
        [dataLoad]="loadDataTable"
        [summaryLoad]="summaryData"
        #jobDataTable>
    </div>