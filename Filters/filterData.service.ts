﻿import { Injectable } from "@angular/core";
import { Headers, Http } from "@angular/http";
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { urlConfig } from "../../Core/DataLayer/dataLayer.config";

@Injectable()
export class FilterDataService {

    fetchSelectData(options: any): Observable<any> {
        let url = urlConfig.apiBase + options.dataSource;
        return this.http.get(url, options.dataParams)
            .map((res) => {
                var list = res.json();
                var result = [];
                for (let item in list) {
                    //console.log(list[item], options.pkidColumnName, list[item][options.pkidColumnName]);
                    result.push({ value: list[item][options.pkidColumnName], text: list[item][options.textColumnName]});
                }
                return result;
            });
            //.catch((err:any) => Observable.throw(err.json().error || 'Server error'));
    }

    constructor(private http: Http) { }
}