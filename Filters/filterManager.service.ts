﻿import { Injectable } from "@angular/core";

import { I_SearchParams } from "../../ORM/SearchParams.orm";
import { UtilityDataService } from "../../../Modules/Utility/Services/Utility.Data.services";
import { I_JSON } from "../../ORM/Utility.orm";

@Injectable()
export class FilterManagerService {
    config: any = {};
    configName: string;
    search: string = "";

    getFilterData(params: I_SearchParams): Promise<any> {
        
        for (let item in this.config.filters) {

            //if (typeof params[this.config.filters[item].iSearchFilterTarget] === "array") {
            //    params[this.config.filters[item].iSearchFilterTarget].push(this.config.filters[item].value);
            //}
            //else {
                params[this.config.filters[item].iSearchFilterTarget] = this.config.filters[item].value;
            //}
        }
        params.Phrase = this.search;
        return Promise.resolve(params);
                
    }

    setConfig(config) {
        
        this.configName = config;
        let json = new I_JSON();
        json.JSON_Title = this.configName;
        return this.utility.fetchJSONByTitle(json)
        .then((config) => this.config = JSON.parse(config.JSON))
    }

    constructor(private utility: UtilityDataService) { }
}