﻿import { Component, Input } from "@angular/core";

@Component({
    selector: "date-filter",
    templateUrl: "app/Shared/DataTable/Filters/Components/Controls/Date/dateFilter.html"
})
export class DateFilterComponent {

    @Input("options") options;

    constructor() { }
}