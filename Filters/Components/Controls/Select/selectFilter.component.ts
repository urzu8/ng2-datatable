﻿import { Component, Input, OnInit } from "@angular/core";

import { FilterDataService } from "../../../filterData.service";

import { LPSelectComponent } from "../../../../../Select/Components/select.component";

@Component({
    selector: "select-filter",
    templateUrl: "app/Shared/DataTable/Filters/Components/Controls/Select/selectFilter.html"
})
export class SelectFilterComponent {
    data: any = [];

    @Input("options") options;
    
    selected(selectedItem: any) {
        let arr = [];
        if(typeof selectedItem.value === "array"){
            for (let i in selectedItem.value){
                arr.push(selectedItem.value[i].value);
            }
        }
        else{
            arr.push(selectedItem.value.value);
        }
        this.options.value = arr;
    }

    ngOnInit(){
        console.log('options', this.options);
    }

    loadData = () => {
        return this.filterDataService.fetchSelectData(this.options);
        
    }

    constructor(private filterDataService: FilterDataService) {}
}