﻿import { Component, Input, OnInit } from "@angular/core";

@Component({
    selector: "filter",
    templateUrl: "app/Shared/DataTable/Filters/Components/Controls/filterBase.html"
})
export class FilterBaseComponent implements OnInit {

    @Input("options") options;

    ngOnInit() {
        console.log("FilterBaseComponent", this.options);
    }

    constructor() { }
}