﻿import { Component, Input } from "@angular/core";

@Component({
    selector: "text-filter",
    templateUrl: "app/Shared/DataTable/Filters/Components/Controls/Text/textFilter.html"
})
export class TextFilterComponent {

    @Input("options") options;

    constructor() { }
}