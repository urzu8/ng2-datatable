﻿import { Component, Input, OnInit } from "@angular/core";

import { FilterManagerService } from "../filterManager.service";

@Component({
    selector: "[filter-list]",
    templateUrl: "app/Shared/DataTable/Filters/Components/filterListComponent.html"
})
export class FilterListComponent implements OnInit {

    @Input("filter-config") filterConfig;
    @Input("on-apply") onApply;

    showFilters: boolean = false;
    defaults: any = {};

    ngOnInit(){
        this.manager.setConfig(this.filterConfig);
        this.saveFilterDefaults();
    }

    saveFilterDefaults(){
        for(let i in this.filterConfig){
            this.defaults[this.filterConfig[i].label] = this.filterConfig[i].value;
        }
    }

    onApplyClicked(){

    }

    constructor(private manager: FilterManagerService) {

        console.log("y u no?");
     }
}